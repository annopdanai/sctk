import random
import sys
import webbrowser
from PySide6.QtWidgets import *
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtMultimedia import QSoundEffect

from tango_constants import *


class DroppingItem:
    def __init__(self,x = 0, y = 0, image = "resource/tango.png"):
        self.imagename = image
        self.image = QPixmap(image)
        self.x = x
        self.y = y
        self.w = 30
        self.h = 30
    
    def draw(self, p):
        p.drawPixmap(QRect(self.x, self.y, self.w, self.h), self.image)
    
    def falling(self):
        self.y += 10

    def warpToDrop(self):
        self.y = 0
        self.x = random.randint(0, 300 - self.w)
        self.imagename = DroppingItem.RNGItem()
        self.image = QPixmap(self.imagename)
        print("DEBUG: Dropping " + self.imagename)

    @staticmethod
    def RNGItem():
        rand = random.randint(1,100)
        if rand >= 99:
            return "resource/seagate.png" # extremely high points.
        elif rand >= 94:
            return "resource/lean.png" #more points as yui hirasawa
        elif rand >= 87:
            return "resource/drone.png" #deduct points unless you play as mozzie
        elif rand >= 75:
            return "resource/astolfo.png" #more points
        elif rand >= 50:
            return "resource/seno.jpg" #funny nadeko snake moment. plays renai circulation when picked up. araragi is immune to this
        else:
            return "resource/tango.png" #basic points

#base class for player
class Player:
    def __init__(self, x = 0, y = 200, image = "resource/luciano.png"):
        self.image = QPixmap(image)
        self.x = x
        self.y = y
        self.w = 50
        self.h = 100
        self.point = 0
    
    def draw(self, p):
        p.drawPixmap(QRect(self.x, self.y, self.w, self.h), self.image)
    
    def score(self, imagename):
        match imagename:
            case "resource/tango.png":
                self.point += 2
            case "resource/astolfo.png":
                self.point += 5
            case "resource/lean.png":
                self.point += 10
            case "resource/seagate.png":
                self.point += 50
            case "resource/drone.png":
                self.point -= 10
            case "resource/seno.jpg":
                webbrowser.open(random.choice(RENAI_CIRCULATION))
                print("DEBUG: Playing Renai Circulation")
            case _:
                raise HowDidYouGetHereException("How did you get here?")
        print("DEBUG: Score: " + str(self.point))
    
    def collision(self, item):
        if (self.x < item.x + item.w and
            self.x + self.w > item.x and
            self.y < item.y + item.h and
            self.y + self.h > item.y):
            return True
        return False

#mozzie is the only one who can get drones
class Mozzie(Player):
    def __init__(self, x = 0, y = 200, image = "resource/mozzie.png"):
        Player.__init__(self, x, y, image)
        self.mozzievoice = QSoundEffect()
        self.mozzievoice.setSource(QUrl.fromLocalFile("resource/noworriesmate.wav"))
        
    def score(self, imagename):
        match imagename:
            case "resource/tango.png":
                self.point += 2
            case "resource/astolfo.png":
                self.point += 5
            case "resource/lean.png":
                self.point += 10
            case "resource/seagate.png":
                self.point += 50
            case "resource/drone.png":
                self.point += 10
                self.mozzievoice.play()
            case "resource/seno.jpg":
                webbrowser.open(random.choice(RENAI_CIRCULATION))
                print("DEBUG: Playing Renai Circulation")
            case _:
                raise HowDidYouGetHereException("How did you get here?")
        print("DEBUG: Score: " + str(self.point))

#yui gets more points for butterfly pea tea
class Yui(Player):
    def __init__(self, x = 0, y = 200, image = "resource/yui.png"):
        Player.__init__(self, x, y, image)
    
    def score(self, imagename):
        match imagename:
            case "resource/tango.png":
                self.point += 2
            case "resource/astolfo.png":
                self.point += 5
            case "resource/lean.png":
                self.point += 40
            case "resource/seagate.png":
                self.point += 50
            case "resource/drone.png":
                self.point -= 10
            case "resource/seno.jpg":
                webbrowser.open(random.choice(RENAI_CIRCULATION))
                print("DEBUG: Playing Renai Circulation")
            case _:
                raise HowDidYouGetHereException("How did you get here?")
        print("DEBUG: Score: " + str(self.point))

#bocchi dies when she loses an item
class Bocchi(Player):
    def __init__(self, x = 0, y = 200, image = "resource/bocchi.png"):
        Player.__init__(self, x, y, image)
        self.scream = QSoundEffect()
        self.scream.setSource(QUrl.fromLocalFile("resource/bocchiscream.wav"))
    
    def score(self, imagename):
        match imagename:
            case "resource/tango.png":
                self.point += 2
            case "resource/astolfo.png":
                self.point += 5
            case "resource/lean.png":
                self.point += 10
            case "resource/seagate.png":
                self.point += 50
            case "resource/drone.png":
                self.point -= 10
            case "resource/seno.jpg":
                webbrowser.open(random.choice(RENAI_CIRCULATION))
                print("DEBUG: Playing Renai Circulation")
            case _:
                raise HowDidYouGetHereException("How did you get here?")
        print("DEBUG: Score: " + str(self.point))

    def melt(self):
        self.scream.play()
        raise BocchiDiesException("Bocchi died") #use this instead as a dangerous placeholder for melt.png

#koyomi-onichan is the only person who can get nadeko. nadeko loves koyomi.
class Araragi(Player):
    def __init__(self, x = 0, y = 200, image = "resource/araragi.png"):
        Player.__init__(self, x, y, image)
    
    def score(self, imagename):
        match imagename:
            case "resource/tango.png":
                self.point += 2
            case "resource/astolfo.png":
                self.point += 5
            case "resource/lean.png":
                self.point += 10
            case "resource/seagate.png":
                self.point += 50
            case "resource/drone.png":
                self.point += 10
            case "resource/seno.jpg":
                self.point += 10
            case _:
                raise HowDidYouGetHereException("How did you get here?")
        print(random.choice(RRRG) + "-San, you now have " + str(self.point) + " points.")

#chisato doesn't get any points. doesn't get any items either. doesn't get anything. she's just there.
#before you open an issue about this, yes, this is intentional.
class Chisato(Player):
    def __init__(self, x = 0, y = 200, image = "resource/chisato.png"):
        Player.__init__(self, x, y, image)
    
    def collision(self, item):
        pass

#spy is invisible
class Spy(Player):
    def __init__(self, x = 0, y = 200, image = ""):
        Player.__init__(self, x, y, image)

class PlayArea(QWidget):
    def __init__(self):
        QWidget.__init__(self, None)
        self.setMinimumSize(300, 300)
        self.playerpos = 0
        self.player = Player()
        self.droppingItem = DroppingItem()
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.updateValue)
        self.timer.start(30)
        self.hitsound = QSoundEffect()
        self.hitsound.setSource(QUrl.fromLocalFile("resource/normal-hitclap.wav"))

    def paintEvent(self, e):
        p = QPainter()
        p.begin(self)
        self.player.draw(p)
        self.droppingItem.draw(p)
        p.end()
    
    def updateValue(self):
        if self.player.collision(self.droppingItem):
            self.player.score(self.droppingItem.imagename)
            self.droppingItem.warpToDrop()
            self.hitsound.play()
        if self.droppingItem.y > 300:
            self.droppingItem.warpToDrop()
            if type(self.player) == Bocchi:
                self.player.melt()
        if self.player.point >= 500:
            self.timer.start(5)
        elif self.player.point >= 400:
            self.timer.start(10)
        elif self.player.point >= 300:
            self.timer.start(15)
        elif self.player.point >= 200:
            self.timer.start(20)
        elif self.player.point >= 100:
            self.timer.start(25)
        else:
            self.timer.start(30)
        self.droppingItem.falling()
        self.update()

class TangoGame(QWidget):
    def __init__(self):
        QWidget.__init__(self, None)
        self.setWindowTitle("Tango Game")
        self.setGeometry(100, 100, 300, 300)
        self.verticalLayout = QVBoxLayout(self)

        self.horiLayout = QHBoxLayout()
        self.charBox = QComboBox()
        self.charBox.addItem("Luciano")
        self.charBox.addItem("Mozzie")
        self.charBox.addItem("Yui")
        self.charBox.addItem("Bocchi")
        self.charBox.addItem("Araragi")
        self.charBox.addItem("Chisato")
        self.charBox.addItem("Spy")
        self.charBox.currentIndexChanged.connect(self.changeChar)
        self.horiLayout.addWidget(self.charBox)

        self.pauseButton = QPushButton("Pause")
        self.pauseButton.clicked.connect(self.pause)
        self.horiLayout.addWidget(self.pauseButton)

        self.verticalLayout.addLayout(self.horiLayout)

        self.playArea = PlayArea()
        self.playArea.move(0, 0)
        self.playArea.resize(300, 300)
        self.verticalLayout.addWidget(self.playArea)

        self.sliding = QSlider(Qt.Horizontal)
        self.sliding.setMinimum(0)
        self.sliding.setMaximum(300)
        self.sliding.setValue(0)
        self.sliding.setTickInterval(10)
        self.sliding.setTickPosition(QSlider.TicksBelow)
        self.sliding.valueChanged.connect(self.sliderChanged)
        self.verticalLayout.addWidget(self.sliding)
        self.show()
    
    def sliderChanged(self, value):
        self.playArea.player.x = value

    def pause(self):
        if self.playArea.timer.isActive():
            self.playArea.timer.stop()
            self.sliding.setDisabled(True)
            self.pauseButton.setText("Resume")
        else:
            self.playArea.timer.start(30)
            self.sliding.setDisabled(False)
            self.pauseButton.setText("Pause")

    def changeChar(self):
        currentpos = self.playArea.player.x
        match self.charBox.currentText():
            case "Luciano":
                self.playArea.player = Player(x = currentpos)
            case "Mozzie":
                self.playArea.player = Mozzie(x = currentpos)
            case "Yui":
                self.playArea.player = Yui(x = currentpos)
            case "Bocchi":
                self.playArea.player = Bocchi(x = currentpos)
            case "Araragi":
                self.playArea.player = Araragi(x = currentpos)
            case "Chisato":
                self.playArea.player = Chisato(x = currentpos)
            case "Spy":
                self.playArea.player = Spy(x = currentpos)
            case _:
                raise HowDidYouGetHereException("How did you get here? Like how even? I don't even know how you got here.")

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = TangoGame()
    sys.exit(app.exec())

import sys
from forex_python.converter import CurrencyRates, RatesNotAvailableError
from PySide6.QtWidgets import *
from PySide6.QtCore import *

class currencyExchange(QWidget):
    def __init__(self):
        QWidget.__init__(self, None)
        self.firstCurr = 0
        self.secCurr = 0

        self.currencylists = ["THB", "USD", "EUR", "JPY", "CNY", "KRW", "SGD", "GBP", "RUB", "INR"]
        self.currRate = CurrencyRates()

        self.setWindowTitle("Currency Exchange")

        self.grid = QGridLayout()

        self.firstBox = QComboBox(self)
        self.firstBox.addItems(self.currencylists)
        self.grid.addWidget(self.firstBox, 0, 0)

        self.firstEdit = QLineEdit(self)
        self.firstEdit.setText("0")
        self.grid.addWidget(self.firstEdit, 0, 1)

        self.secondBox = QComboBox(self)
        self.secondBox.addItems(self.currencylists)
        self.grid.addWidget(self.secondBox, 1, 0)

        self.secondEdit = QLineEdit(self)
        self.secondEdit.setText("0")
        self.grid.addWidget(self.secondEdit, 1, 1)

        self.firstButton = QPushButton(self)
        self.firstButton.setText("Convert down")
        self.firstButton.clicked.connect(self.first)
        self.grid.addWidget(self.firstButton, 0, 2)

        self.secondButton = QPushButton(self)
        self.secondButton.setText("Convert up")
        self.secondButton.clicked.connect(self.second)
        self.grid.addWidget(self.secondButton, 1, 2)


        self.nameLabel = QLabel(self)
        self.nameLabel.setText("Currency Exchange")
        self.nameLabel.setAlignment(Qt.AlignCenter)

        self.vbox = QVBoxLayout()
        self.vbox.addWidget(self.nameLabel)
        self.vbox.addLayout(self.grid)

        self.setLayout(self.vbox)

        self.show()

    def first(self):
        try:
            currrate = self.currRate.get_rate(self.firstBox.currentText(), self.secondBox.currentText())
            self.secondEdit.setText(str(float(self.firstEdit.text()) * currrate))
        except RatesNotAvailableError:
            self.errorMessage = self.errorMessage()
            self.errorMessage.show()

    def second(self):
        try:
            currrate = self.currRate.get_rate(self.firstBox.currentText(), self.secondBox.currentText())
            self.firstEdit.setText(str(float(self.secondEdit.text()) / currrate))
        except RatesNotAvailableError:
            self.errorMessage = self.errorMessage()
            self.errorMessage.show()

    class errorMessage(QMessageBox):
        def __init__(self):
            QMessageBox.__init__(self, None)
            self.setWindowTitle("Error on obtaining the currency rate")
            self.setText("Error on obtaining the currency rate. The API might be down or something.")

if __name__ == "__main__":
    app = QApplication(sys.argv)

    window = currencyExchange()

    sys.exit(app.exec())
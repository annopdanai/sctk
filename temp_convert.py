import sys
from PySide6.QtWidgets import *
from PySide6.QtCore import *
from tango_constants import HowDidYouGetHereException

#all of these converts from and to kelvin, because kelvin is the base unit. cry about it.
toCelsius = lambda x: x - 273.15
toFahrenheit = lambda x: (x - 273.15) * 9/5 + 32
toRankine = lambda x: x * 9/5
toReaumur = lambda x: (x - 273.15) * 4/5

fromReumur = lambda x: x * 5/4 + 273.15
fromFahrenheit = lambda x: (x - 32) * 5/9 + 273.15
fromRankine = lambda x: x * 5/9
fromCelsius = lambda x: x + 273.15

class TempConverter(QWidget):
    def __init__(self):
        QWidget.__init__(self, None)
        self.kelvin = 0 #kelvin is the base unit. cry about it.
        self.templist = ["Kelvin", "Celsius", "Fahrenheit", "Rankine","Reaumur"]
        self.setWindowTitle("Temperature Converter")

        self.grid = QGridLayout()

        self.firstBox = QComboBox(self)
        self.firstBox.addItems(self.templist)
        self.grid.addWidget(self.firstBox, 0, 0)
    
        self.firstEdit = QLineEdit(self)
        self.firstEdit.setText("0")
        self.grid.addWidget(self.firstEdit, 0, 1)

        self.secondBox = QComboBox(self)
        self.secondBox.addItems(self.templist)
        self.grid.addWidget(self.secondBox, 1, 0)

        self.secondEdit = QLineEdit(self)

        self.secondEdit.setText("0")

        self.grid.addWidget(self.secondEdit, 1, 1)

        self.firstButton = QPushButton(self)
        self.firstButton.setText("Convert down")
        self.firstButton.clicked.connect(self.first)
        self.grid.addWidget(self.firstButton, 0, 2)

        self.secondButton = QPushButton(self)
        self.secondButton.setText("Convert up")
        self.secondButton.clicked.connect(self.second)
        self.grid.addWidget(self.secondButton, 1, 2)

        self.nameLabel = QLabel(self)
        self.nameLabel.setText("Temperature Converter")
        self.nameLabel.setAlignment(Qt.AlignCenter)

        self.vbox = QVBoxLayout()
        self.vbox.addWidget(self.nameLabel)
        self.vbox.addLayout(self.grid)

        self.setLayout(self.vbox)

        self.show()

    def first(self):
        try:
            match self.firstBox.currentText():
                case "Kelvin":
                    self.kelvin = float(self.firstEdit.text())
                case "Celsius":
                    self.kelvin = fromCelsius(float(self.firstEdit.text()))
                case "Fahrenheit":
                    self.kelvin = fromFahrenheit(float(self.firstEdit.text()))
                case "Rankine":
                    self.kelvin = fromRankine(float(self.firstEdit.text()))
                case "Reaumur":
                    self.kelvin = fromReumur(float(self.firstEdit.text()))
                case _:
                    raise HowDidYouGetHereException("How did you get here?")

            match self.secondBox.currentText():
                case "Kelvin":
                    self.secondEdit.setText(str(self.kelvin))
                case "Celsius":
                    self.secondEdit.setText(str(toCelsius(self.kelvin)))
                case "Fahrenheit":
                    self.secondEdit.setText(str(toFahrenheit(self.kelvin)))
                case "Rankine":
                    self.secondEdit.setText(str(toRankine(self.kelvin)))
                case "Reaumur":
                    self.secondEdit.setText(str(toReaumur(self.kelvin)))
                case _:
                    raise HowDidYouGetHereException("How did you get here?")
        except ValueError:
            self.secondEdit.setText("Error")
    
    def second(self):
        try:
            match self.secondBox.currentText():
                case "Kelvin":
                    self.kelvin = float(self.secondEdit.text())
                case "Celsius":
                    self.kelvin = fromCelsius(float(self.secondEdit.text()))
                case "Fahrenheit":
                    self.kelvin = fromFahrenheit(float(self.secondEdit.text()))
                case "Rankine":
                    self.kelvin = fromRankine(float(self.secondEdit.text()))
                case "Reaumur":
                    self.kelvin = fromReumur(float(self.secondEdit.text()))
                case _:
                    raise HowDidYouGetHereException("How did you get here?")

            match self.firstBox.currentText():
                case "Kelvin":
                    self.firstEdit.setText(str(self.kelvin))
                case "Celsius":
                    self.firstEdit.setText(str(toCelsius(self.kelvin)))
                case "Fahrenheit":
                    self.firstEdit.setText(str(toFahrenheit(self.kelvin)))
                case "Rankine":
                    self.firstEdit.setText(str(toRankine(self.kelvin)))
                case "Reaumur":
                    self.firstEdit.setText(str(toReaumur(self.kelvin)))
                case _:
                    raise HowDidYouGetHereException("How did you get here?")
        except ValueError:
            self.firstEdit.setText("Error")

if __name__ == "__main__":
    app = QApplication(sys.argv)
    temp = TempConverter()
    sys.exit(app.exec())